#!/bin/bash

#apt-get -y update

# quiet installation
export DEBIAN_FRONTEND=noninteractive

## LibreOffice base
## LibreOffice mysql c driver
## java driver for mariadb
apt-get -y install libreoffice-base libreoffice-sdbc-mysql libmariadb-java
