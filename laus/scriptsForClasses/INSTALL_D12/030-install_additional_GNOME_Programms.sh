#!/bin/bash

#apt-get -y update

# quiet installation
export DEBIAN_FRONTEND=noninteractive

## gnome-poer-manager: GUI to control battery status
## meld grafical diff tool
## dconf-editor: GUI to view gsettings
apt-get -y install gnome-power-manager meld dconf-editor
