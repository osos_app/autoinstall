#!/bin/bash

# SOURCE: https://itslinuxfoss.com/install-xrdp-server-remote-desktop-debian-12/
# apt-get -y update

# quiet installation
export DEBIAN_FRONTEND=noninteractive

## Install the XRDP
apt-get -y install xrdp 

## Restart the XRDP
systemctl restart xrdp
