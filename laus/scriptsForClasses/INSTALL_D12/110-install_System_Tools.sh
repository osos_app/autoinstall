#!/bin/bash

#apt-get -y update

# quiet installation
export DEBIAN_FRONTEND=noninteractive

## net-tools:  This includes arp, ifconfig, netstat,
##             rarp, nameif and route. Additionally, this package contains utilities
##             relating to particular network hardware types (plipconfig, slattach,
##             mii-tool) and advanced aspects of IP configuration (iptunnel, ipmaddr).
## mc: Midnight - Commander
## htop: HTop Konsole Systemmonitor
## tree: Tree Konsole Filetree Viewer
## git: Version Control
## gitk: GUI tool to view git-history
## exfat-fuse: add exfat - filesystem
## rsync: to sync user.save with user and vm's
## stacer: system monitoring
apt-get -y install net-tools mc htop tree git gitk exfat-fuse rsync stacer


