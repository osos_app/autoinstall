#!/bin/bash

# Extrakt File 
# with
# FILENAME
# from
# SOURCE_PATH
# to 
# DESTINATION_PATH

# Source Laus-Settings
. /etc/default/laus-setup

SOURCE_PATH=${MOUNT_PATH_ON_CLIENT}/xBigFiles

apt-get -y install ${SOURCE_PATH}/Virtualbox-7/virtualbox-7.0_7.0.12.deb


#VBoxManage extpack uninstall "Oracle VM VirtualBox Extension Pack"
# Get licence-key with 
# >> VBoxmanage extpack install Oracle_VM_VirtualBox_Extension_Pack-....
# from Konsole
# version 1:
# VBoxManage extpack install /tmp/${file} --replace --accept-license=56be48f923303c8cababb0bb4c478284b688ed23f16d775d729b89a2e8e5f9eb
# version 2: new license key: 2021.03.06 and same key on 2023.11.20
VBoxManage extpack install ${SOURCE_PATH}/Virtualbox-7/Oracle_VM_VirtualBox_Extension_Pack-7.0.12.vbox-extpack --replace --accept-license=33d7284dc4a0ece381196fda3cfe2ed0e1e8e7ed7f27b9a9ebc4ee22e24bd23c


