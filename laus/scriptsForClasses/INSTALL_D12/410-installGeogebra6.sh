#!/bin/bash

# Source Laus-Settings
# Extrakt File 
# with
# FILENAME
# from
# SOURCE_PATH
# to 
# DESTINATION_PATH

# Source Laus-Settings
. /etc/default/laus-setup

SOURCE_PATH=$MOUNT_PATH_ON_CLIENT/xBigFiles

if [ ! "$(ls /usr/bin/geogebra-classic)" == "/usr/bin/geogebra-classic" ];
then
	echo "Install Geogebra"
	apt-get -y install $SOURCE_PATH/geogebra.deb
else
	echo "Geogebra already installed"
fi
