#!/bin/bash

#apt-get -y update

# quiet installation
export DEBIAN_FRONTEND=noninteractive

## Inkscape, free vector graphic 
apt-get -y install inkscape
