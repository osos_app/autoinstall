#!/bin/bash

#apt-get -y update

# quiet installation
export DEBIAN_FRONTEND=noninteractive

## LibreOffice full Package
## LibreOffice German Languagepackage
## LibreOffice java connection needed e.g. language-tool
## LibreOffice Englisch-GB, German-AT, German-DE, Italy, France, Spain Dictionarys
## Hyphenation & Thesaurus
apt-get -y install libreoffice-l10n-de libreoffice-java-common hunspell hunspell-de-at hunspell-de-de hunspell-en-gb hunspell-it hunspell-fr hunspell-fr-classical hunspell-es
