#!/bin/bash

# Source Laus-Settings
. /etc/default/laus-setup

## purge english thesaurus due to english examinee requirements

apt-get -y purge mythes-en-us
