#!/bin/bash

#apt-get -y update

# quiet installation
export DEBIAN_FRONTEND=noninteractive

## C++ - Programming - Stuff: build-essential
## Editors: geany bluefish 
## MESA 3D Grafic Library: libgl1-mesa-dev
## Android Debug Bridge, e.g. to bring apks into anbox: android-tools-adb
apt-get -y install build-essential geany bluefish libgl1-mesa-dev android-tools-adb

## Java - Default development Kit
apt-get -y install default-jdk

## Python - Programming Stuff
apt-get -y install python-is-python3 python3-matplotlib
