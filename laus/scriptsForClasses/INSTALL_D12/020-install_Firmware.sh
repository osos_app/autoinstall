#!/bin/bash

#apt-get -y update

# quiet installation
export DEBIAN_FRONTEND=noninteractive

## from https://forums.debian.net/viewtopic.php?t=151785
apt-get -y install isenkram-cli

isenkram-autoinstall-firmware

# it looks like isenkram-autoinstall-firmware returns an error, if nothing todo
# therefore we place a dummy command after it
echo "finished isenkram-autoinstall-firmware"
