#! /bin/bash

file="/etc/gdm3/daemon.conf"
if ! test -f $file".original"; then
	cp $file $file".original"
fi
updatetime=$(date +%Y%m%d-%T)
newfile=$file".laus."$updatetime
cp $file $newfile

## change from Wayland back to X11
## because 
## if machine is used with WLAN 
## and programm (e.g firefox, libreoffice, ...) is opened 
## immediately after login by examinee-user, graphic freezes.
## possible reason: iptables stop network and restart it
## some machine with LAN, does not have this problem 
sed -e "{
	/WaylandEnable/ s/#//
}" -i $file
