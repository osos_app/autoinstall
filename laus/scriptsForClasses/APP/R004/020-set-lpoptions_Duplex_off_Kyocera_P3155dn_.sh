#!/bin/bash

PRINTER_NAME="Raum-004-Printer"

echo "Dest ${PRINTER_NAME} Duplex=None
" >> /etc/cups/lpoptions

chown root:lp /etc/cups/lpoptions
chmod 664 /etc/cups/lpoptions
