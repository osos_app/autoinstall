#!/bin/bash

# Source Laus-Settings
. /etc/default/laus-setup

## block certain network-devices for NetworkManager
##
## from: https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html/configuring_and_managing_networking/configuring-networkmanager-to-ignore-certain-devices_configuring-and-managing-networking
## and
## https://wiki.debian.org/NetworkInterfaceNames

echo "
[keyfile]
unmanaged-devices=interface-name:enx*
" > /etc/NetworkManager/conf.d/99-unmanaged-devices.conf

systemctl restart NetworkManager