#!/bin/bash

##
## it looks like systemd-networking is not bringing up interfaces
## right in time, so that LAUS will find its server, when graphic 
## is installed.
## So we enable NetworkManager for all interfaces
## HINT: 
## Desktop PCs with normal LAN interface
## are now handled the same way LAPTOPs are
## On laptops NetworkManager is necessary
## to change between LAN & WLAN on demand
##

## disable first networkdevice in /etc/network/interfaces
file="/etc/network/interfaces"
if ! [ -f ${file}".original" ];
then
	cp ${file} ${file}".original"
fi
updatetime=$(date +%Y%m%d-%T)
newfile=${file}".laus."$updatetime
cp ${file} ${newfile}

sed -e "{
	/allow-hotplug/ s/allow-hotplug/#allow-hotplug/
}" -i ${file}

sed -e "{
	/dhcp/ s/^/#/
}" -i ${file}


## enable Networkmanager

file="/etc/NetworkManager/NetworkManager.conf"
if ! [ -f ${file}".original" ];
then
	cp ${file} ${file}".original"
fi
updatetime=$(date +%Y%m%d-%T)
newfile=${file}".laus."$updatetime
cp ${file} ${newfile}

sed -e "{
	/managed=false/ s/managed=false/managed=true/
}" -i ${file}

