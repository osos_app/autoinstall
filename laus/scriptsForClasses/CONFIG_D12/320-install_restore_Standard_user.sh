#!/bin/bash

. /etc/default/laus-setup
SOURCE_PATH=$MOUNT_PATH_ON_CLIENT/xBigFiles

## create shell file in /usr/local/bin,
## which restores standard user
echo "#!/bin/bash

# Restore Standard - User
# replace standard user configuration with saved version

rsync -a --delete --chown=user:user /home/user.save/ /home/user/

# set Displayname back to user, if it has been changed
sed '/user:x:1234/ s/user:x:1234.*/user:x:1234:1234:user:\/home\/user:\/bin\/bash/' -i /etc/passwd

" > /usr/local/bin/restoreStandardUser.sh

chmod 755 /usr/local/bin/restoreStandardUser.sh


## create service file, which starts restores standard user service
## in /lib/systemd/system
echo "[Unit]
Description=Restore Standard User service
Requires=local-fs.target
After=local-fs.target
Before=laus.service

[Service]
Type=simple
ExecStart=/usr/local/bin/restoreStandardUser.sh

[Install]
WantedBy=multi-user.target
" > /lib/systemd/system/restore-standard-user.service

chmod 755 /lib/systemd/system/restore-standard-user.service

systemctl enable restore-standard-user.service


## get user.save from git & set rights for standard user profile
cd /home
## clone user - repository from git
git clone http://git01/user.save.12
## change name from user.save.12 to user.save
mv user.save.12 user.save
