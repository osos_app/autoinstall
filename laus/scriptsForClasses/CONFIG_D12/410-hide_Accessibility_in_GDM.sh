#!/bin/bash

file="/etc/gdm3/greeter.dconf-defaults"
if ! [ -f $file".original" ];
then
    cp $file $file".original"
fi
updatetime=$(date +%Y%m%d-%T)
newfile=$file".laus."$updatetime
cp $file $newfile

## Hide Accessibility menu in GDM, because zooming, freezes screens on 
## some PCs with X11 and beamers
##
## so we add option 
##
## [org/gnome/desktop/a11y]
## always-show-universal-access-status=false
##
## after # primary-color='#000000' to preserve 
## [org/gmome/desktop] structure
## ATTENTION:
## script depends on line: 
## # primary-color='#000000'
## for inserting :-(
## TODO:
## find better solution for inserting
if ! grep "always-show-universal-access-status=false" $file;
then
    sed -e "{
        s/# primary-color='#000000'/# primary-color='#000000'\n[org\/gnome\/desktop\/a11y]\nalways-show-universal-access-status=false/
    }"  -i $file
fi


