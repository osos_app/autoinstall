#!/bin/bash

if [ -f /usr/local/bin/clean_and_restore.sh ];
then
	echo "
# CLEAN:
echo \"clean directory /media from orphaned user-name mount points\"
rmdir /media/*

" >> /usr/local/bin/clean_and_restore.sh
else
	echo "file /usr/local/bin/clean_and_restore.sh NOT found => exit 1"
	exit 1
fi
