#!/bin/bash

file="/etc/default/grub"
if ! [ -f $file".original" ];
then
	cp $file $file".original"
fi
updatetime=$(date +%Y%m%d-%T)
newfile=$file".laus."$updatetime
cp $file $newfile


## String ersetzen
sed '/GRUB_TIMEOUT=5/ s/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/' -i $file

update-grub
