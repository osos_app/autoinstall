#!/bin/bash

file="/etc/gdm3/greeter.dconf-defaults"
if ! [ -f $file".original" ];
then
	cp $file $file".original"
fi
updatetime=$(date +%Y%m%d-%T)
newfile=$file".laus."$updatetime
cp $file $newfile

## change idle-timeout from 20min to 60min
sed -e "{
	/# sleep-inactive-ac-timeout=1200/ s/# sleep-inactive-ac-timeout=1200/sleep-inactive-ac-timeout=3600/
}" -i $file

## change action for idle-timeout from 'suspend' to 'shutdown'
sed -e "{
	/# sleep-inactive-ac-type='suspend'/ s/# sleep-inactive-ac-type='suspend'/sleep-inactive-ac-type='shutdown'/
}" -i $file


