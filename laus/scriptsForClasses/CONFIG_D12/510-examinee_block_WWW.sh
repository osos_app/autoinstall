#!/bin/bash

## Script to disable/enable WWW on examinee login/logout

#  we need iptables to block internet addresses
apt-get -y install iptables

## EXPLANATION:
#  Installscript to add a hooked_script = ${HOOKED_SCRIPT} to
#  GDM PreSession default run script, called on LOGIN
#  GDM PostSession default run script, called on LOGOUT
#
#  hooked_script shall react on parameters start/stop
#  so
#  >>hooked_script start
#  will be called on gui-session login
#  and
#  >>hooked_script stop
#  will be called gui-session logout
#
#  for logic of hoocked_script look at ${HOOKED_SCRIPT}
#  here in directory files or in /usr/local/bin/${HOOKED_SCRIPT}
#  in an allready installed machine
#
#  ATTENTION:
#  on shutdown PostSession run script is NOT EXECUTED so:
#  persistant changes have to be reverted in script:
#  /usr/local/bin/clean_and_restore.sh
#  which is installed from laus-script: 120-install_clean_and_restore_service.sh

# hocked_script for this
HOOKED_SCRIPT="blockWWWForExaminees.sh"
# white list file, containing URLs reachable from examinee enviroment
WWW_WHITE_LIST="whitelist.lst"

#  ADDITIONAL actions in /usr/local/bin/clean_and_restore.sh
#  to restore standard:
#  actions: NONE

# PreSession
CONF_DIR_PRE="/etc/gdm3/PreSession"
CONF_FILE_PRE="Default"
# PostSession
CONF_DIR_POST="/etc/gdm3/PostSession"
CONF_FILE_POST="Default"

# Copy hoocked-script to /usr/local/bin
cp  files/${HOOKED_SCRIPT} /usr/local/bin/
chmod 750 /usr/local/bin/${HOOKED_SCRIPT}

# Copy whitelist to /usr/local/bin
cp  files/${WWW_WHITE_LIST} /usr/local/bin/
chmod 750 /usr/local/bin/${WWW_WHITE_LIST}

# Add hook $HOOKED_SCRIPT to file ${CONF_DIR_PRE}/${CONF_FILE_PRE}
echo "
# Hook to /usr/local/bin/${HOOKED_SCRIPT}
/bin/bash /usr/local/bin/${HOOKED_SCRIPT} start
" >> ${CONF_DIR_PRE}/${CONF_FILE_PRE}

# disable exit 0 in ${CONF_DIR_POST}/${CONF_FILE_POST}
# before adding own hook
sed -e "{
    /exit 0/ s/^exit 0/#exit 0/
}" -i ${CONF_DIR_POST}/${CONF_FILE_POST}

# Add hook $HOOKED_SCRIPT to file ${CONF_DIR_POST}/${CONF_FILE_POST}
echo "
# Hook to /usr/local/bin/${HOOKED_SCRIPT}
/bin/bash /usr/local/bin/${HOOKED_SCRIPT} stop
" >> ${CONF_DIR_POST}/${CONF_FILE_POST}

