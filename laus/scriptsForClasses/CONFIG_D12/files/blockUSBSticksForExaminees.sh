#!/bin/bash

# DISABLE - ENABLE /media for Examinees

if [[ $USER == examinee* ]];
then
	case "$1" in
	start)
		# disable /media
		chmod 750 /media
		;;
	stop)
		# enable /media
		chmod 755 /media
		;;
	esac
fi


