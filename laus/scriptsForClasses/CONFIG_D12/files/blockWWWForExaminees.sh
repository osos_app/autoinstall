#!/bin/bash

# DISABLE - ENABLE WWW for Examinees

if [[ $USER == examinee* ]];
then
    case "$1" in
    start)
        # disable WWW on examinee LOGIN
        # set Policy DROP for chain OUTPUT
        /sbin/iptables -P OUTPUT DROP

        # Insert rule ACCEPT for destination 127.0.0.0/24
        /sbin/iptables -I OUTPUT -d 127.0.0.0/24 -j ACCEPT

        # Insert rule ACCEPT for destination 10.0.0.0/8
        /sbin/iptables -I OUTPUT -d 10.0.0.0/8 -j ACCEPT

        # Insert rule ACCEPT for URLs in whitelist.lst
        for url in $(cat /usr/local/bin/whitelist.lst);
        do
            /sbin/iptables -I OUTPUT -d ${url} -j ACCEPT
        done
        ;;
    stop)
        # enable WWW on examinee LOGOUT
        /sbin/iptables -P OUTPUT ACCEPT
        /sbin/iptables -F
        ;;
    esac
fi


