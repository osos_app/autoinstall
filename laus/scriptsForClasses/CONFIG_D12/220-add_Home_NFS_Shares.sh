#!/bin/bash

## Fileservers for HOME-directories
FS_HOME_TEACHERS_701016="hole01"
FS_HOME_PUPILS_701016="hosu01"
FS_HOME_V_701016="hole01"
FS_HOME_EXAMINEES_701016="hole01"

FS_HOME_TEACHERS_701036="hole01"
FS_HOME_PUPILS_701036="hosu01"
FS_HOME_V_701036="hole01"
FS_HOME_EXAMINEES_701036="hole01"

FS_HOME_TEACHERS_701076="hole01"
FS_HOME_PUPILS_701076="hosu02"
FS_HOME_V_701076="hole01"
FS_HOME_EXAMINEES_701076="hole01"

# quiet installation
export DEBIAN_FRONTEND=noninteractive

apt-get -y install nfs-common autofs

## Configuration autofs
file="/etc/auto.master"
if ! [ -f $file".original" ];
then
    cp $file $file".original"
fi
updatetime=$(date +%Y%m%d-%T)
newfile=$file".laus."$updatetime
cp $file $newfile


## append path of config files
## for auto mountpoints to /etc/auto.master
echo "
#
# HOME - directories für teachers AGI
/home/users/701016/l    /etc/auto.users.701016.l        --ghost
#
# HOME - directories für teachers BRG
/home/users/701036/l    /etc/auto.users.701036.l        --ghost
#
# HOME - directories für teachers GfB
/home/users/701076/l    /etc/auto.users.701076.l        --ghost
#
# HOME - directories für pupils AGI
/home/users/701016/s    /etc/auto.users.701016.s        --ghost
#
# HOME - directories für pupils BRG
/home/users/701036/s    /etc/auto.users.701036.s        --ghost
#
# HOME - directories für pupils GfB
/home/users/701076/s    /etc/auto.users.701076.s        --ghost
#
# HOME - directories für Verwaltung AGI
/home/users/701016/v    /etc/auto.users.701016.v        --ghost
#
# HOME - directories für Verwaltung BRG
/home/users/701036/v    /etc/auto.users.701036.v        --ghost
#
# HOME - directories für Verwaltung GfB
/home/users/701076/v    /etc/auto.users.701076.v        --ghost
#
# RETURN - directories für Examinees AGI
/home/users/701016/e    /etc/auto.users.701016.e        --ghost
#
# RETURN - directories für Examinees BRG
/home/users/701036/e    /etc/auto.users.701036.e        --ghost
#
# RETURN - directories für Examinees GfB
/home/users/701076/e    /etc/auto.users.701076.e        --ghost
#
" >> /etc/auto.master

## sec = krb5 will be taken from NFS - Server
## that meens: if NFS - server exports with sec=krb5
## client will mount with sec=krb5
## if NFS - server exports without sec=krb5
## client will mount without sec=krb5
##
## create config file for teachers AGI
echo "
*   -fstype=nfs4        ${FS_HOME_TEACHERS_701016}:/701016/l/&
" > /etc/auto.users.701016.l

## create config file for teachers BRG
echo "
*   -fstype=nfs4        ${FS_HOME_TEACHERS_701036}:/701036/l/&
" > /etc/auto.users.701036.l

## create config file for teachers GfB
echo "
*   -fstype=nfs4        ${FS_HOME_TEACHERS_701076}:/701076/l/&
" > /etc/auto.users.701076.l

## create config file for Verwaltung AGI
echo "
*   -fstype=nfs4        ${FS_HOME_V_701016}:/701016/v/&
" > /etc/auto.users.701016.v

## create config file for Verwaltung BRG
echo "
*   -fstype=nfs4        ${FS_HOME_V_701036}:/701036/v/&
" > /etc/auto.users.701036.v

## create config file for Verwaltung GfB
echo "
*   -fstype=nfs4        ${FS_HOME_V_701076}:/701076/v/&
" > /etc/auto.users.701076.v

## create config file for pupils AGI
echo "
*   -fstype=nfs4        ${FS_HOME_PUPILS_701016}:/701016/s/&
" > /etc/auto.users.701016.s

## create config file for pupils BRG
echo "
*   -fstype=nfs4        ${FS_HOME_PUPILS_701036}:/701036/s/&
" > /etc/auto.users.701036.s

## create config file for pupils GfB
echo "
*   -fstype=nfs4        ${FS_HOME_PUPILS_701036}:/701076/s/&
" > /etc/auto.users.701076.s

## Examinees 
## we can NOT use wildcard, because teachers have to see all examinees
## for controlling and collecting

## create config file for Examinees AGI
echo "
examinees   -fstype=nfs4        ${FS_HOME_EXAMINEES_701016}:/701016/e
" > /etc/auto.users.701016.e

## create config file for Examinees BRG
echo "
examinees   -fstype=nfs4        ${FS_HOME_EXAMINEES_701036}:/701036/e
" > /etc/auto.users.701036.e

## create config file for Examinees GfB
echo "
examinees   -fstype=nfs4        ${FS_HOME_EXAMINEES_701076}:/701076/e
" > /etc/auto.users.701076.e

systemctl restart autofs
