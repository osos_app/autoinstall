#!/bin/bash

. /etc/default/laus-setup

SOURCE_PATH=$MOUNT_PATH_ON_CLIENT/xBigFiles

## create shell file, which cleans and restore used machines
## in /usr/local/bin
## IMPORTANT: #!/bin/bash has to be in FIRST LINE
## shell in systemd does not expand *
## therefore ExecStart=rmdir /media/* DOES NOT WORK!!!
## and command explaced in an extra script
echo "#!/bin/bash

## script to collect actions to clean & restore machine from
## left over changes made by usage

" > /usr/local/bin/clean_and_restore.sh

chmod -R 755 /usr/local/bin/clean_and_restore.sh


## create service file, which starts clean-and-restore service
## in /lib/systemd/system
echo "[Unit]
Description=Clean and restore machine from changes made by usage
Requires=local-fs.target
After=local-fs.target

[Service]
Type=simple
ExecStart=/usr/local/bin/clean_and_restore.sh

[Install]
WantedBy=multi-user.target

" > /lib/systemd/system/clean-and-restore.service

systemctl enable clean-and-restore.service

