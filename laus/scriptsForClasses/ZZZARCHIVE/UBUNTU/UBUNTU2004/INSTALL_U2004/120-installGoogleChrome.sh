#!/bin/bash

# Extrakt File 
# with
# FILENAME
# from
# SOURCE_PATH
# to 
# DESTINATION_PATH

# Source Laus-Settings
. /etc/default/laus-setup

SOURCE_PATH=$MOUNT_PATH_ON_CLIENT/xBigFiles

apt-get -y install $SOURCE_PATH/google-chrome-stable_current_amd64.deb

# deb will add an repository
# we do an update and install latest version
apt-get -y update

apt-get -y install google-chrome-stable
