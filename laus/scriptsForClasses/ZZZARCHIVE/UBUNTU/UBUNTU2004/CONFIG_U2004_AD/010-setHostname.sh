#!/bin/bash

if [[ $(hostname) != $(cat /etc/hostname) ]];
then
	echo "we set hostname $(hostname) for this host"
	hostnamectl set-hostname $(hostname)
fi
