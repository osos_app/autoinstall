#!/bin/bash

file="/etc/update-manager/release-upgrades"
if ! [ -f $file".original" ];
then
	cp $file $file".original"
fi
updatetime=$(date +%Y%m%d-%T)
newfile=$file".laus."$updatetime
cp $file $newfile

sed '/Prompt=lts/ s/Prompt=lts/Prompt=never/' -i $file
