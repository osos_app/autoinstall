#! /bin/bash

# Source Laus-Settings
. /etc/default/laus-setup

SOURCE_PATH=$MOUNT_PATH_ON_CLIENT/xBigFiles

apt-get -y update
#  apt-get install gstreamer1.0-vaapi (For Intel graphics)
apt-get -y install libssl-dev libavahi-compat-libdnssd-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev gstreamer1.0-libav gstreamer1.0-vaapi

# create 
# desktop-file : /usr/share/applications/app-airplay.desktop
# with link to 
# shell-script : /usr/local/bin/airplay
# to start airplay after login
echo "[Desktop Entry]
Type=Application
Version=1.0
Keywords=airplay;Airplay;
Categories=Utility;Application;
Comment=Airplay
Comment[de]=Airplay
Exec=/usr/local/bin/airplay
GenericName[de]=Airplay
GenericName=Airplay
Icon=phone.png
MimeType=
Name=Airplay
Name[de]=Airplay
Path=/usr/local/bin/
StartupNotify=true
Terminal=true
TerminalOptions=
" > /usr/share/applications/app-airplay.desktop

# copy uxplay and set permissions
cp -v ${SOURCE_PATH}/uxplay/uxplay /usr/local/bin
chmod 755 /usr/local/bin/uxplay

# create - airplay - Shortcutfile
echo "
#!/bin/bash

uxplay -n $(hostname)

" > /usr/local/bin/airplay

chmod 755 /usr/local/bin/airplay

