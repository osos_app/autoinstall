#!/bin/bash

# Source Laus-Settings
. /etc/default/laus-setup

# create desktop file in: 
# /usr/share/applications
# Windows 11 in a VirtualBox Machine
DESKTOPFILE=win11.desktop
DESKTOPFILEPATH=/usr/share/applications

echo "[Desktop Entry]
Version=1.0
Type=Application
Terminal=true
Icon=/opt/vms/win11.png
Exec=/opt/vms/startVM.sh /opt/vms/win11.vdi --firmware=efi
Categories=Application
Comment=Startet Windows 11 in einer VirtualBox Maschine
Comment[de]=Starts Windows 11 in a VirtualBox Machine
GenericName=Windows 11
GenericName[de]=Windows 11
Name=Windows 11
Name[de]=Windows 11
StartupNotify=true
" > $DESKTOPFILEPATH/$DESKTOPFILE

