#!/bin/bash

## Add user as dummy to server

# original:
## user:x:3101:2000:user:/home/user:/bin/bash

echo "user:x:1234:1234:user:/temp:/usr/sbin/nologin" >> /etc/passwd

## Add groups with user 'user' to server

echo "school-lehrerschuelersnr701036:x:300090511:user" >> /etc/group
echo "school-lehrerschuelersnr701076:x:300094932:user" >> /etc/group
